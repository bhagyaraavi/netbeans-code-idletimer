package idlemonitor.controller;

import java.util.Date;


import org.apache.log4j.Logger;

import idlemonitor.common.ReadConfigureProperties;
import idlemonitor.common.WebServiceRequest;

import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;
/**
 * 
 * @author Bhagya
 * class for to handle the shutdown sevrices
 */
public class ShutdownServices extends Thread{
	private static final Logger log= Logger.getLogger(ShutdownServices.class);
	
	private  ReadConfigureProperties readConfigureProperties=new ReadConfigureProperties();
	String baseUrl=readConfigureProperties.getBaseUrl();
	 WebServiceRequest webServiceRequest=new WebServiceRequest();
	 private Integer userId=0;
	 ShutdownServices(Integer userId) {
			this.userId=userId;
		}
	public void run(){  
		log.info("ShutdownServices -> run() "+ new Date());
    	System.out.println(" Application Shutdown "+ new Date());
        // this is executed on shut-down. put whatever.
    	//IdleMonitor.saveWorkHours();
    	JSONObject shutdownInputJson=new JSONObject();
    	try {
    		shutdownInputJson.accumulate("userId", userId);
    		shutdownInputJson.accumulate("description", "shutdown");
    		shutdownInputJson.accumulate("dataId", 0);
		} catch (JSONException e) {
			log.info("ShutdownServices -> run() -> JSONException "+e.toString());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	StringBuilder shutdownStringBuilder = new StringBuilder(baseUrl);
   		shutdownStringBuilder.append("clientactivity/saveorupdateuserloginsinfo.do");
   		String shutdownRequestUrl =shutdownStringBuilder.toString();
     	String shutdownResult=webServiceRequest.POSTRequest(shutdownRequestUrl, shutdownInputJson.toString(),userId);
     	Integer shutdownResultId=0;
     	if(null!=shutdownResult){
     		try {
     		JSONObject shutdownjsonResult = new JSONObject(shutdownResult);
     		String shutdownstatus=shutdownjsonResult.getString("status");
     		if(shutdownstatus.equalsIgnoreCase("success")){
     			shutdownResultId=shutdownjsonResult.getInt("dataId");
     			System.out.println("shutdown ResultId "+shutdownResultId);
     		}
 			} catch (JSONException e) {
 				log.info("ShutdownServices -> run() -> After Post request ->JSONException "+e.toString());
 				// TODO Auto-generated catch block
 				e.printStackTrace();
 			}
     	}
        log.info("shut down hook task completed..");  
    }  
}