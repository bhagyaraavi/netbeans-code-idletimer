/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package idlemonitor.appUIScreens;

import javax.swing.DefaultComboBoxModel;
import org.quartz.Job;

/**
 *
 * @author Bhagya
 */
public class MyComboBoxModel extends DefaultComboBoxModel<Job> {
    public MyComboBoxModel(Job[] items) {
        super(items);
    }
 
    @Override
    public Job getSelectedItem() {
        Job selectedJob = (Job) super.getSelectedItem();
 
        // do something with this job before returning...
 
        return selectedJob;
    }
}
