package idlemonitor.utility.scheduler;

import org.apache.log4j.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import idlemonitor.appUIScreens.LoginForm;
/***
 * 
 * @author Bhagya
 * 
 * Scheduler class for to save user work hours
 */
public class SaveWorkHoursScheduler{
	private static final Logger log= Logger.getLogger(SaveWorkHoursScheduler.class);
	SchedulerFactory factory = new StdSchedulerFactory();
	Scheduler scheduler=null;
	Boolean isSchedulerShutdown=false;
	Boolean isSchdeulerProcess=false;
	/**
	 * Created By Bhagya
	 * Scheduler class for to save user work hours
	 */
	public void schedulerService(){
		log.info(" SaveWorkHoursScheduler -> schedulerService()");
		System.out.println(" Inside start scheduler service requests");
		try {
			
				 JobDetail job = JobBuilder.newJob(SaveWorkHoursJob.class)
				.withIdentity("saveWorkHoursJob", "group1").build();
		
		    	Trigger trigger = TriggerBuilder
				.newTrigger()
				.withIdentity("saveWorkHoursTriggerName", "group1")
				.withSchedule(
					CronScheduleBuilder.cronSchedule("0 1-59/10 * * * ?")) // trigger has been set to run at every 10 minutes
				.build();
		    	//schedule it
    	
    		
    		if(isSchdeulerProcess==false || isSchedulerShutdown==true) {
	    		scheduler =getSchdeulerObject();
	    		scheduler.start();
	    		scheduler.scheduleJob(job, trigger);
	    		isSchdeulerProcess=true;
    		}
    		
			
		} 
		catch (SchedulerException e) {
			log.info(" SaveWorkHoursScheduler -> SchedulerException -> "+e.toString());
			
		}
    	catch(Exception e) {
    		log.info(" SaveWorkHoursScheduler -> Exception -> "+e.toString());
    		e.printStackTrace();
    	}

    }
	/**
	 * Created By bhagya
	 * This will shutdown the scheduler object
	 */
	public void stopSchedulerService() {
		log.info(" SaveWorkHoursScheduler -> stopSchedulerService() ");
			try {
				
				if(scheduler!=null) {
					boolean waitForJobsToComplete = true;
			        scheduler.shutdown(waitForJobsToComplete);
			        isSchedulerShutdown=scheduler.isShutdown();
			        System.out.println("Scheduler shutdown? "+ scheduler.isShutdown());
				}
			} 
			catch (SchedulerException e) {
				log.info(" SaveWorkHoursScheduler -> stopSchedulerService() -> SchedulerException -> "+e.getMessage());
				e.printStackTrace();
			}
			catch(Exception e) {
	    		log.info(" SaveWorkHoursScheduler -> stopSchedulerService() -> Exception -> "+e.toString());
	    		e.printStackTrace();
	    	}
	}
	
	/**
	 * 
	 * Created By Bhagya
	 * Getting the scheduler object from scheduler factory
	 */
	public Scheduler getSchdeulerObject() {
		log.info(" SaveWorkHoursScheduler -> getSchdeulerObject() ");
		try {
			 scheduler =factory.getScheduler();
		}
		catch (SchedulerException e) {
			log.info(" SaveWorkHoursScheduler -> getSchdeulerObject() -> SchedulerException -> "+e.getMessage());
			e.printStackTrace();
		}
		return scheduler;
	}
	/**
	 * 
	 *Created By Bhagya
	 *Service will return the existed scheduler object
	 */
	public Scheduler getExistedSchedulerObject() {
		log.info(" SaveWorkHoursScheduler -> getExistedSchedulerObject()");
		return scheduler;
	}
}