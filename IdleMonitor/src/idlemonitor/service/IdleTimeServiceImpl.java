package idlemonitor.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import idlemonitor.common.ReadConfigureProperties;
import idlemonitor.common.WebServiceRequest;

import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;

/**
 * Created By Bhagya
 * Implementation class for idle timer services 
 */
public class IdleTimeServiceImpl implements IdleTimeService{
	private static final Logger log= Logger.getLogger(IdleTimeServiceImpl.class);
	
	 WebServiceRequest webServiceRequest=new WebServiceRequest();
	 private  ReadConfigureProperties readConfigureProperties=new ReadConfigureProperties();
	 String baseUrl=readConfigureProperties.getBaseUrl();
	
	/**
	 * Created By Bhagya
	 * Service request for to get idle minutes
	 */
	
	public String getIdleMinutesByUserIdFromWebService(Integer userId){
		log.info(" IdleTimeServiceImpl -> getIdleMinutesByUserIdFromWebService() ");
		StringBuilder stringBuilder = new StringBuilder(baseUrl);
   		stringBuilder.append("clientactivity/getidleminutes.do");
   		String requestUrl =stringBuilder.toString();
   		JSONObject inputJson=new JSONObject();
   		try {
			inputJson.accumulate("userId", userId);
		} catch (JSONException e1) {
			log.info(" IdleTimeServiceImpl -> getIdleMinutesByUserIdFromWebService() -> JSONException -> "+e1.toString());
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
     	String resultString=webServiceRequest.POSTRequest(requestUrl, inputJson.toString(),userId);
     	String idleMinutes="5";
     	if(null!=resultString){
     		try {
     		JSONObject jsonResult = new JSONObject(resultString);
     		String status=jsonResult.getString("status");
     		if(status.equalsIgnoreCase("success")){
     			idleMinutes=jsonResult.getString("idleMinutes");
     		}
 			} catch (JSONException e) {
 				log.info(" IdleTimeServiceImpl -> getIdleMinutesByUserIdFromWebService() -> after post request-> JSONException -> "+e.toString());
 				// TODO Auto-generated catch block
 				e.printStackTrace();
 			}
     	}
     	return idleMinutes;
	}
	/**
	 * Created By Bhagya 
	 * Service request for to update the user status info
	 */
	public void updateUserStatusInfoFromWebService(Integer userId,String status){
		log.info(" IdleTimeServiceImpl -> updateUserStatusInfoFromWebService ()");
		StringBuilder userStatus = new StringBuilder(baseUrl);
   		userStatus.append("clientactivity/updatestatus.do");
   		String userStatusRequestUrl =userStatus.toString();
   		JSONObject userStatusinputJson=new JSONObject();
   		try {
   			userStatusinputJson.accumulate("userId", userId);
   			userStatusinputJson.accumulate("status", status);
		} catch (JSONException e1) {
			log.info(" IdleTimeServiceImpl -> updateUserStatusInfoFromWebService () -> JSONException -> "+e1.toString());
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
     	String userStatusResultString=webServiceRequest.POSTRequest(userStatusRequestUrl, userStatusinputJson.toString(),userId);
     	Integer userStatusUpdatedResult=0;
     	if(null!=userStatusResultString){
     		try {
     		JSONObject userStatusjsonResult = new JSONObject(userStatusResultString);
     		String userStatusValue=userStatusjsonResult.getString("status");
     		if(userStatusValue.equalsIgnoreCase("success")){
     			userStatusUpdatedResult=userStatusjsonResult.getInt("resultId");
     		}
 			} catch (JSONException e) {
 				log.info(" IdleTimeServiceImpl -> updateUserStatusInfoFromWebService ()-> after post request -> JSONException -> "+e.toString());
 				// TODO Auto-generated catch block
 				e.printStackTrace();
 			}
     	}
	}
	/**
	 * Created By BHagya
	 *  Service for to save user login info
	 */
	public Integer saveUserLoginInfoFromWebService(Integer userId){
		log.info(" IdleTimeServiceImpl -> saveUserLoginInfoFromWebService() ");
		Integer dataId=0;
		try{
			StringBuilder userLoginStatus = new StringBuilder(baseUrl);
	   		userLoginStatus.append("clientactivity/saveorupdateuserloginsinfo.do");
	   		String userLoginStatusRequestUrl =userLoginStatus.toString();
	   		JSONObject userLoginStatusinputJson=new JSONObject();
	   		try {
	   			userLoginStatusinputJson.accumulate("userId", userId);
	   			userLoginStatusinputJson.accumulate("description", "login");
	   			userLoginStatusinputJson.accumulate("dataId", 0);
			} catch (JSONException e1) {
				log.info(" IdleTimeServiceImpl -> saveUserLoginInfoFromWebService() -> JSONException -> "+e1.toString()); 
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	     	String userLoginStatusResultString=webServiceRequest.POSTRequest(userLoginStatusRequestUrl, userLoginStatusinputJson.toString(),userId);
	     	
	     	if(null!=userLoginStatusResultString){
	     		try {
	     		JSONObject userLoginStatusjsonResult = new JSONObject(userLoginStatusResultString);
	     		String userLoginStatusValue=userLoginStatusjsonResult.getString("status");
	     		if(userLoginStatusValue.equalsIgnoreCase("success")){
	     			dataId=userLoginStatusjsonResult.getInt("dataId");
	     		}
	 			} catch (JSONException e) {
	 				log.info(" IdleTimeServiceImpl -> saveUserLoginInfoFromWebService()-> After psot request -> JSONException -> "+e.toString()); 
	 				// TODO Auto-generated catch block
	 				e.printStackTrace();
	 			}
	     	}
			
		
		 
		}
		catch(Exception e){
			
		}
		return dataId;
	}
	/**
	 * Created By Bhagya
	 * Service request for to save user idle status info
	 */
	public void saveUserIdleStatusInfoFromWebService(Integer userId,Integer dataId){
		log.info("IdleTimeServiceImpl -> saveUserIdleStatusInfoFromWebService () ");
		try{
			StringBuilder userIdleStatus = new StringBuilder(baseUrl);
	   		userIdleStatus.append("clientactivity/saveorupdateuserloginsinfo.do");
	   		String userIdleStatusRequestUrl =userIdleStatus.toString();
	   		JSONObject userIdleStatusinputJson=new JSONObject();
	   		try {
	   			userIdleStatusinputJson.accumulate("userId", userId);
	   			userIdleStatusinputJson.accumulate("description", "idle");
	   			userIdleStatusinputJson.accumulate("dataId", dataId);
			} catch (JSONException e1) {
				log.info("IdleTimeServiceImpl -> saveUserIdleStatusInfoFromWebService () -> JSONException -> "+e1.toString());
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	     	String userIdleStatusResultString=webServiceRequest.POSTRequest(userIdleStatusRequestUrl, userIdleStatusinputJson.toString(),userId);
	     	Integer resultId=0;
	     	if(null!=userIdleStatusResultString){
	     		try {
	     		JSONObject userIdleStatusjsonResult = new JSONObject(userIdleStatusResultString);
	     		String userIdleStatusValue=userIdleStatusjsonResult.getString("status");
	     		if(userIdleStatusValue.equalsIgnoreCase("success")){
	     			resultId=userIdleStatusjsonResult.getInt("dataId");
	     		}
	 			} catch (JSONException e) {
	 				log.info("IdleTimeServiceImpl -> saveUserIdleStatusInfoFromWebService () -> After psot request-> JSONException -> "+e.toString());
	 				// TODO Auto-generated catch block
	 				e.printStackTrace();
	 			}
	     	}
			
		  //idleTimeService.updateUserLoginsInfo(userDto);
		  log.info("Updated  user Idle status info sucessfully");
		}
		catch(Exception e){
			log.error("Error While Updating user info "+e.getMessage());
		}
	}
	/**
	 * Created bY BHagya
	 * Service request for to get time worked by userId 
	 */
	
	public Map<String,Object> getTimeWorkedsByUserIdFromWebService(Integer userId){
		log.info(" IdleTimeServiceImpl -> getTimeWorkedsByUserIdFromWebService() ");
		Map<String,Object> resultMap=new HashMap<String,Object>();
		try {
			StringBuilder stringBuilder = new StringBuilder(baseUrl);
	   		stringBuilder.append("clientactivity/gettimeworkedbyuser.do");
	   		String requestUrl =stringBuilder.toString();
	   		JSONObject inputJson=new JSONObject();
	   		try {
				inputJson.accumulate("userId", userId);
			} catch (JSONException e1) {
				log.info(" IdleTimeServiceImpl -> getTimeWorkedsByUserIdFromWebService() -> JSONException -> "+e1.toString());
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	     	String resultString=webServiceRequest.POSTRequest(requestUrl, inputJson.toString(),userId);
	     	
	     	if(null!=resultString && resultString.length()>0){
	     		try {
	     		JSONObject jsonResult = new JSONObject(resultString);
	     		String status=jsonResult.getString("status");
	     		resultMap.put("status",status);
	     		//System.out.println("status of time worked "+status);
	     		if(status.equalsIgnoreCase("success")){
	     			resultMap.put("date", jsonResult.getString("date"));
	     			resultMap.put("timeWorked", jsonResult.getString("timeWorked"));
	     		}
	 			} catch (JSONException e) {
	 				log.info(" IdleTimeServiceImpl -> getTimeWorkedsByUserIdFromWebService()-> After post request -> JSONException -> "+e.toString());
	 				// TODO Auto-generated catch block
	 				e.printStackTrace();
	 			}
	     		
	     	}
	     
	     
		}
		catch(Exception e) {
			log.error("Error While getTimeWorkedsByUserIdFromWebService "+e.getMessage());
		}
		return resultMap;
	}
	
	/**
	 * Craeted By Bhagya On AUgust 25th, 2020
	 * @param userId
	 * @return
	 * Service for to update user work hours flag value
	 */
	
	public Map<String,Object> updateUserWorkHoursFlag(Integer userId){
		log.info("IdleTimeServiceImpl -> updateUserWorkHoursFlag ()");
		StringBuilder stringBuilder = new StringBuilder(baseUrl);
   		stringBuilder.append("clientactivity/updateuserworkhoursflag.do");
   		String requestUrl =stringBuilder.toString();
   		JSONObject inputJson=new JSONObject();
   		try {
			inputJson.accumulate("userId", userId);
		} catch (JSONException e1) {
			log.info("IdleTimeServiceImpl -> updateUserWorkHoursFlag () -> JSONException -> "+e1.toString());
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
     	String resultString=webServiceRequest.POSTRequest(requestUrl, inputJson.toString(),userId);
     	Map<String,Object> resultMap=new HashMap<String,Object>();
     	if(null!=resultString){
     		try {
     		JSONObject jsonResult = new JSONObject(resultString);
     		String status=jsonResult.getString("status");
     		if(status.equalsIgnoreCase("success")){
     			resultMap.put("resultId", jsonResult.getInt("resultId"));
     			
     		}
 			} catch (JSONException e) {
 				log.info("IdleTimeServiceImpl -> updateUserWorkHoursFlag ()-> after post request -> JSONException -> "+e.toString());
 				// TODO Auto-generated catch block
 				e.printStackTrace();
 			}
     		
     	}
     
     	return resultMap;
	}
        
        
       
    
}