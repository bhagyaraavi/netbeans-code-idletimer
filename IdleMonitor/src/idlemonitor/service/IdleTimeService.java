package idlemonitor.service;

import java.util.Map;

/**
 * 
 * @author Bhagya
 * Interface class for idle time service
 */
public interface IdleTimeService{
	
	//web services
	
	public String getIdleMinutesByUserIdFromWebService(Integer userId);
	public void updateUserStatusInfoFromWebService(Integer userId,String status);
	public Integer saveUserLoginInfoFromWebService(Integer userId);
	public void saveUserIdleStatusInfoFromWebService(Integer userId,Integer dataId);
	public Map<String,Object> getTimeWorkedsByUserIdFromWebService(Integer userId);
	public Map<String,Object> updateUserWorkHoursFlag(Integer userId);
       
        
}