package idlemonitor.controller;
import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import org.apache.log4j.Logger;

import idlemonitor.common.WebServiceRequest;
import idlemonitor.utility.scheduler.SaveWorkHoursScheduler;
import idlemonitor.service.IdleTimeService;
import idlemonitor.service.IdleTimeServiceImpl;
import com.sun.jna.Native;
import com.sun.jna.Structure;
import com.sun.jna.win32.StdCallLibrary;
import idlemonitor.appUIScreens.WorkedHoursIM;


/**
 * Created By Bhagya
 * This class records time/play time in the format of HH:mm:ss
 */
public class TestClass extends Thread {
	private static final Logger log= Logger.getLogger(RecordTimer.class);
	

	static IdleTimeService idleTimeService=new IdleTimeServiceImpl();
	public  static JLabel labelRecordTime;
	public static JLabel timeWorkedLabelText;
	public static JLabel dateLabelText;
	private Integer userId=0;
        public JLabel testingLabel;
	private static CaptureScreenshots captureScreenshot;
	Boolean isProcess=true;
	static Boolean stopThread=true;
	public static Boolean stopScheduler=false;
        WorkedHoursIM workedHoursIM=new WorkedHoursIM();
        Map<String,Object> LoginDataMap=null;
					
	public TestClass(JLabel labelRecordTime,Integer userId,Map<String,Object> resultMap) {
            System.out.println(" timeworked label text "+timeWorkedLabelText +" userId "+userId);
            LoginDataMap=resultMap;
	    this.userId=userId;
                
	}
	public void stopRunning(){
		log.info(" Record timer -> inside stop running()");
		stopThread=false;
		this.isProcess=false;
		stopScheduler=true;
		captureScreenshot.stopRunning();
	
	}
	public void startRunning(Integer userId){
		log.info(" Record timer -> inside start running()");
		// initializing the capture screenshots thread
		captureScreenshot=new CaptureScreenshots(userId,true);
		captureScreenshot.start();
		captureScreenshot.startRunning();
		stopThread=true;
		this.isProcess=true;
		stopScheduler=false;
	
	}
	 
	/**
	 * Utility method to retrieve the idle time on Windows and sample code to test it.
	 * JNA shall be present in your classpath for this to work (and compile).,
	 * @author ochafik
	 */
	
		public interface Kernel32 extends StdCallLibrary {
			Kernel32 INSTANCE = (Kernel32)Native.loadLibrary("kernel32", Kernel32.class);
			/**
			 * Retrieves the number of milliseconds that have elapsed since the system was started.
			 * @see http://msdn2.microsoft.com/en-us/library/ms724408.aspx
			 * @return number of milliseconds that have elapsed since the system was started.
			 */
			public int GetTickCount();
		};
		public interface User32 extends StdCallLibrary {
			User32 INSTANCE = (User32)Native.loadLibrary("user32", User32.class);
			/**
			 * Contains the time of the last input.
			 * @see http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winui/winui/windowsuserinterface/userinput/keyboardinput/keyboardinputreference/keyboardinputstructures/lastinputinfo.asp
			 */
			public static class LASTINPUTINFO extends Structure {
				public int cbSize = 8;
				/// Tick count of when the last input event was received.
				public int dwTime;
			}
			/**
			 * Retrieves the time of the last input event.
			 * @see http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winui/winui/windowsuserinterface/userinput/keyboardinput/keyboardinputreference/keyboardinputfunctions/getlastinputinfo.asp
			 * @return time of the last input event, in milliseconds
			 */
			public boolean GetLastInputInfo(LASTINPUTINFO result);
		};
		/**
		 * Get the amount of milliseconds that have elapsed since the last input event
		 * (mouse or keyboard)
		 * @return idle time in milliseconds
		 */
		public static int getIdleTimeMillisWin32() {
			User32.LASTINPUTINFO lastInputInfo = new User32.LASTINPUTINFO();
			User32.INSTANCE.GetLastInputInfo(lastInputInfo);
			return Kernel32.INSTANCE.GetTickCount() - lastInputInfo.dwTime;
		}
		enum State {
			UNKNOWN, ONLINE, IDLE, AWAY
		};
		static long displayHours=0;
		static long  displayMinutes=0;
		static long secondspassed=0;
		static Boolean isProcessScreenshots=true;
		static String idleMinutes="5";
		boolean x=true;
		
	public void run() {
            
           
		
				//idleMinutes=idleTimeService.getIdleMinutesByUserIdFromWebService(userId);
				log.info(" inside handling time -> idle minutes "+idleMinutes);
				
				// initializing the capture screenshots thread
				captureScreenshot=new CaptureScreenshots(userId,true);
				captureScreenshot.start();
				
	           long starttime=System.currentTimeMillis();
			   State state = State.UNKNOWN;	// default state
			 
			   //initialize the default values
			   Boolean cancelIdleState=false;			
			   Integer dataId=0;			
			   Long idlesecondspassed=null;			 
			   Long lastSeconds=null;
			  
				for (;;) {
                                    
					int sec = getIdleTimeMillisWin32() / 1000;
					State currentState =sec < 1* 60 ? State.ONLINE :  //seconds
                                        sec > Integer.valueOf(idleMinutes)* 60 ? State.IDLE : State.AWAY;
								
					if(currentState.name().equalsIgnoreCase("ONLINE") ){
						
						                                      
                                                 if(LoginDataMap!=null){
                                                          //  System.out.println(" Inside if login data result"+LoginDataMap);
                                                            WorkedHoursIM.dateLabelText.setText(" Date: "+LoginDataMap.get("date"));
                                                            WorkedHoursIM.timeWorkedLabelText.setText(" Worked Time: "+LoginDataMap.get("timeWorked"));
                                                    }
                                                 else if(isProcess){
                                                     Map<String,Object> resultMap=idleTimeService.getTimeWorkedsByUserIdFromWebService(userId);
                                                     System.out.println(" User Online STtate "+resultMap);
                                                     if(!resultMap.isEmpty()) {
						
                                                    /**
                                                     * Implement the following if condition for to handle the issue of
                                                     * if user didn't close the app by end of day, the previous day hours is coming along with the next day hours
                                                     */
					
							//if(resultMap.get("status").equals("fail")) {
							//System.out.println(" inside result map empty");
							//log.info(" inside result map empty codition - first online checking");
							SimpleDateFormat dateFormat=new SimpleDateFormat("MM/dd/yyyy");
							String currentDateText=dateFormat.format(new Date());
                                                            System.out.println(" date label "+dateLabelText); 
                                                            
							workedHoursIM.dateLabelText.setText(" Date: "+currentDateText);
							workedHoursIM.timeWorkedLabelText.setText(" Worked Time: 00:00:00");
							workedHoursIM.labelRecordTime.setText("Working Time: 00:00:00");
							//initialize the default values
							   dataId=0;			
							   idlesecondspassed=null;			 
							   lastSeconds=null;
							   displayHours=0;
							   displayMinutes=0;
							   secondspassed=0;
							//}
                                                        
						}
                                        }
						idlesecondspassed=secondspassed;
						isProcess=true;
						stopScheduler=false;
						//System.out.println(" idleSconds passed "+idlesecondspassed);
						
						while(isProcess)
					    {
							// if any network error will comes while running scheduler or capture screenshots means, we stopping the timer also by using this condition	
							if(RecordTimer.stopThread==false || captureScreenshot.isJoptionMessageExist==true) {
								
								isProcess=false;
								stopScheduler=true;
								
							}
							
							
							boolean trigger=false; // using this trigger values for to increment the minutes or hours value based on seconds passed
				
							Long timepassed=System.currentTimeMillis()-starttime;
							
					        secondspassed=timepassed/1000;
					       
					       // following two if statements for to control the idle seconds time.
					       if(null!=idlesecondspassed  ){
					    	   	secondspassed=idlesecondspassed;
					    	    starttime=System.currentTimeMillis();  
					    	    idlesecondspassed=null;
					       } 
					       if(secondspassed>60){
					    	   secondspassed=lastSeconds;
					    	   starttime=System.currentTimeMillis();
					       }
					       
					        if(secondspassed==60)
					        {
					        	trigger=true;
					        	 secondspassed=(long) 0;
					            starttime=System.currentTimeMillis();
					        }
					        if(trigger==true && (secondspassed%60)==0){
					        	displayMinutes++;
					        }
					       
					       if(displayMinutes==60){
					    	   displayMinutes=0;
					    	   starttime=System.currentTimeMillis();
					       }
							
					       if(trigger==true &&(displayMinutes%60)==0){ 
					    	   displayHours++;
					    	}
					     
					    System.out.println(displayHours+"::"+displayMinutes+"::"+secondspassed);
					    
					    lastSeconds=secondspassed;
			
					    String displayHoursText=""+displayHours;
					    String displayMinutesText=""+displayMinutes;
					    String displaySecondsText=""+secondspassed;
					    
					    // Following conditions will handle the display timer text
					    if(displayHours<10){
					    	displayHoursText="0"+displayHours;
					    }
					    if(displayMinutes<10){
					    	displayMinutesText="0"+displayMinutes;
					    }
					    if(secondspassed<10){
					    	displaySecondsText="0"+secondspassed;
					    }
					    
					    workedHoursIM.labelRecordTime.setText("Working Time: "+displayHoursText+":"+displayMinutesText+":"+displaySecondsText);
					    
						int idleSec = getIdleTimeMillisWin32() / 1000;
						State newState =
							idleSec < 1* 60 ? State.ONLINE :  //seconds
							idleSec > Integer.valueOf(idleMinutes)* 60 ? State.IDLE : State.AWAY;
							
							
						if (newState != state) {
							System.out.println(" RecordTimer -> Inside If condition -> state "+state +" new state "+newState);
							
							log.info("RecordTimer -> Inside If condition -> state "+state +" new state "+newState);
							
							String oldstatus=state.name();
							state = newState;				
						
							//idleTimeService.updateUserStatusInfoFromWebService(userId, state.name());
							
													
							if(state.name().equalsIgnoreCase("ONLINE") && !oldstatus.equalsIgnoreCase("AWAY") && cancelIdleState==false){
									
								dataId=idleTimeService.saveUserLoginInfoFromWebService(userId);
								captureScreenshot.startRunning();
								stopScheduler=false;
							}
							
							if(state.name().equalsIgnoreCase("IDLE")){
								try {
									log.info(" Inside the idle state Logic ");
									Runtime r=Runtime.getRuntime();
									String message = "The system will going to idle in 30 seconds. Select CANCEL if you won't.";
								    JLabel lbmsg = new JLabel(message);
								    boolean result = showConfirmDialogWithTimeout(lbmsg, "Idle Time Warning", 30 * 1000);
	
								    if (result == false) {
								    	cancelIdleState=true; //cancel is selected
								    	log.info("RecordTimer -> UserAction -> Cancel the system IDLE State ");
								    }
								    else 
								    {
								    	cancelIdleState=false; //okay is selected
								    	
								    	log.info("RecordTimer -> UserAction-> Set The system to IDLE State ");
								    	
								    	captureScreenshot.stopRunning();
										idleTimeService.saveUserIdleStatusInfoFromWebService(userId, dataId);
										stopScheduler=true;
										if(secondspassed>=50){ 
                                                                                    secondspassed=(long) 0;
										}
										isProcess=false;
										idlesecondspassed=secondspassed;
										lastSeconds=secondspassed;
										r.exec("shutdown -h"); //set the system to hibernate/sleep mode
										
								    }
									
								} catch (IOException e) {
									log.info(" Record Timer -> IOException -> "+e.toString());
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								catch (Exception e) {
									log.info(" Record Timer -> Exception -> "+e.toString());
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							
						}
						try { 
							Thread.sleep(1000);
							} 
						catch (Exception ex) {}
						
					}//while
				}//if
			       
				
			} // for
		
	}//method
	/**
	 * Created By Bhagya
	 * @param params
	 * @param title
	 * @param timeout_ms
	 * @return
	 * 
	 * This service will handle the popup alert to user about the idle state
	 */
	public final static boolean showConfirmDialogWithTimeout(Object params, String title, int timeout_ms) {
		log.info(" RecordTimer -> showConfirmDialogWithTimeout() ");
		String[] options = {"CANCEL"};
	    final JOptionPane msg = new JOptionPane(params, JOptionPane.WARNING_MESSAGE, JOptionPane.CANCEL_OPTION,null, options , options[0]);
	    final JDialog dlg = msg.createDialog(title);

	    msg.setInitialSelectionValue(JOptionPane.OK_OPTION);
	    dlg.setAlwaysOnTop(true);
	    dlg.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	    dlg.addComponentListener(new ComponentAdapter() {
	        @Override
	        public void componentShown(ComponentEvent e) {
	            super.componentShown(e);
	            final Timer t = new Timer(timeout_ms, new ActionListener() {
	                @Override
	                public void actionPerformed(ActionEvent e) {
	                    dlg.setVisible(false);
	                }
	            });
	            t.start();
	        }
	    });
	    dlg.setVisible(true);

	    Object selectedvalue = msg.getValue();
	   
	    if (selectedvalue.equals("CANCEL")) {
	        return false;
	    } else {
	        return true;
	    }
	}
	
}