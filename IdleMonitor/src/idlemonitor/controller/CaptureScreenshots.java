package idlemonitor.controller;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.imageio.ImageIO;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import idlemonitor.common.MultipartUtility;
import idlemonitor.common.ReadConfigureProperties;
import idlemonitor.common.WebServiceRequest;
import idlemonitor.utility.scheduler.SaveWorkHoursScheduler;
import idlemonitor.service.IdleTimeService;
import idlemonitor.service.IdleTimeServiceImpl;

import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;


/**
 * This class captures the screenshots of pc/laptop
 * @author Bhagya
 *
 */
public class CaptureScreenshots extends Thread {
	private static final Logger log= Logger.getLogger(CaptureScreenshots.class);
	
	private  ReadConfigureProperties readConfigureProperties=new ReadConfigureProperties();
	private  WebServiceRequest webServcieRequest=new WebServiceRequest();
	 String baseUrl=readConfigureProperties.getBaseUrl();
	 Boolean isJoptionMessageExist=false;
	 public static Boolean stopScheduler=false;
	SaveWorkHoursScheduler saveWorkHoursScheduler=new SaveWorkHoursScheduler();

	
	private Integer userId=0;
	Boolean isProcess=true;
	public CaptureScreenshots(Integer userId,Boolean isProcess) {
		this.userId=userId;
		this.isProcess=isProcess;
	}
	public void stopRunning() {
		log.info(" CaptureScreenshots -> StopRunning()");
		saveWorkHoursScheduler.stopSchedulerService();
		this.isProcess=false;
	}
	public void startRunning(){
		log.info(" CaptureScreenshots -> StartRunning()");
		this.isProcess=true;
	}
	public void run() {
		
		while (isProcess) {
			
			  try { 
				  this.saveUserCaptureScreenImage(userId); 
				} 
			  catch (AWTException e1) {
				//TODO Auto-generated catch block 
				  e1.printStackTrace();
			  }
		 
			
			try {
				//Thread.sleep(5000);
				Thread.sleep((long)(Math.random() * 600000));
				//It will sleep for something between 0 and 60000 milliseconds =1 minute.
				//											600000 milliseconds=10 minutes,300000=5minutes,180000=3minutes
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}//while
			
		
	}
	/***
	 * Created By Bhagya
	 * @param userId
	 * @throws AWTException
	 * 
	 * Service to save the user screen capture image
	 * Modified By bhagya on September 10th, 2020
	 * Added the network handling dialog and conditions
	 * If service Request was not successful, showing network warning message
	 * And verifying the network is available by every 30 seconds
	 * once it is available ,requesting same service from the catch blocks
	 */
	
	/*
	 * public void saveUserCaptureScreenImage(Integer userId) throws AWTException{
	 * log.info("Inside CaptureScreenshots -> saveUserCaptureScreenImage() -> user"
	 * +userId); System.out.println(" Inside capture save"); String charset =
	 * "UTF-8"; Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	 * Rectangle screenRectangle = new Rectangle(screenSize); Robot robot = new
	 * Robot(); BufferedImage image = robot.createScreenCapture(screenRectangle);
	 * String filepath="/src/com/kns/main/captureImages/image1.png"; try {
	 * 
	 * File destinationFile=new File(filepath); if(!destinationFile.exists()){
	 * destinationFile.mkdirs(); }
	 * 
	 * ImageIO.write(image, "png", destinationFile);
	 * 
	 * String requestURL = baseUrl+"clientactivity/savescreencaptureimage.do" ;
	 * 
	 * File uploadFile1 = new File(filepath); MultipartUtility multipart = new
	 * MultipartUtility(requestURL,charset);
	 * 
	 * multipart.addHeaderField("User-Agent", "CodeJava");
	 * multipart.addHeaderField("Test-Header", "Header-Value"); JSONObject json =
	 * new JSONObject(); json.put("userId",userId); multipart.addJSON(json);
	 * multipart.addFilePart("files", uploadFile1); List<String> response =
	 * multipart.finish(); if(isJoptionMessageExist==true) {
	 * webServcieRequest.closeDialog(); isJoptionMessageExist=false; startRunning();
	 * } } catch(ConnectException e){ System.out.println(" connection exception ");
	 * webServcieRequest.closeDialog(); isJoptionMessageExist=true; stopRunning();
	 * log.
	 * info("CaptureScreenshots -> saveUserCaptureScreenImage() -> ConnectException ->"
	 * +e.getMessage()+" stacktrace "+e.getStackTrace()); String
	 * message="Either Your Network Problem or Server Refused Your Request";
	 * webServcieRequest.dialoghandling(message);
	 * this.saveUserCaptureScreenImage(userId);
	 * 
	 * 
	 * } catch (MalformedURLException e) {
	 * log.info(" WebServiceRequest -> GetRequest -> MalformedURLException ->"+e.
	 * getMessage()); e.printStackTrace();
	 * 
	 * } catch (IOException ex) { System.out.println(" IO Exception ");
	 * webServcieRequest.closeDialog(); isJoptionMessageExist=true; stopRunning();
	 * log.
	 * error("CaptureScreenshots -> saveUserCaptureScreenImage() -> IOException ->"
	 * +ex); String
	 * message="You are not connected to network.Pleace check your network connection"
	 * ; webServcieRequest.dialoghandling(message);
	 * this.saveUserCaptureScreenImage(userId);
	 * 
	 * } catch (Exception ex) { ex.printStackTrace();
	 * log.error("CaptureScreenshots -> saveUserCaptureScreenImage()-> Exception ->"
	 * +ex);
	 * 
	 * } finally{ ImageIO.setUseCache(false); } }
	 */
	
	public  void saveUserCaptureScreenImage(Integer userId) throws AWTException{
		log.info("Inside CaptureScreenshots -> saveUserCaptureScreenImage() -> user"+userId);
		System.out.println(" Inside capture save");
		String charset = "UTF-8";
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Rectangle screenRectangle = new Rectangle(screenSize);
		Robot robot = new Robot();
		BufferedImage image = robot.createScreenCapture(screenRectangle);
		String filepath="/src/com/kns/main/captureImages/image1.png";
		try {
			
			  File destinationFile=new File(filepath);
			  if(!destinationFile.exists()){
			  destinationFile.mkdirs();
			  }
				
		    ImageIO.write(image, "png", destinationFile);
		
		String requestURL = baseUrl+"clientactivity/savescreencaptureimage.do" ;

		File uploadFile1 = new File(filepath);
		MultipartUtility multipart = new MultipartUtility(requestURL,charset);

			multipart.addHeaderField("User-Agent", "CodeJava");
			multipart.addHeaderField("Test-Header", "Header-Value");
			JSONObject json = new JSONObject();
			json.put("userId",userId);	
			multipart.addJSON(json);
			multipart.addFilePart("files", uploadFile1);
            List<String> response = multipart.finish();
            if(null!=response){
	        		try {
	            		JSONObject jsonResult = new JSONObject(response.get(0));
	            		String status=jsonResult.getString("STATUS");
	            		if(status.equalsIgnoreCase("success")){
	            			System.out.println(" Inside status success");
	            		}
	            		
	    			} 
	        		
	        		catch (JSONException e) {
	        			
	    				e.printStackTrace();
	    			}
	        	}
				/*
				 * if(isJoptionMessageExist==true) { webServcieRequest.closeDialog();
				 * isJoptionMessageExist=false; startRunning(); }
				 */
		}
		catch(ConnectException e){
			System.out.println(" connection exception ");
			//webServcieRequest.closeDialog();
			//isJoptionMessageExist=true;
			//stopRunning();
			//log.info("CaptureScreenshots -> saveUserCaptureScreenImage() -> ConnectException ->"+e.getMessage()+" stacktrace "+e.getStackTrace());
			//String message="Either Your Network Problem or Server Refused Your Request";
			//webServcieRequest.dialoghandling(message);
		   // this.saveUserCaptureScreenImage(userId);
			saveScreenCapturesLocal(image,userId);
  			
  		}
		catch (MalformedURLException e) {
			log.info(" WebServiceRequest -> GetRequest -> MalformedURLException ->"+e.getMessage());
	  		e.printStackTrace();

	  	  } 
		 catch (IOException ex) {
			 System.out.println(" IO Exception ");
			//webServcieRequest.closeDialog();
			//isJoptionMessageExist=true;
			//stopRunning();
			//log.error("CaptureScreenshots -> saveUserCaptureScreenImage() -> IOException ->"+ex);
			//String message="You are not connected to network.Pleace check your network connection";
			//webServcieRequest.dialoghandling(message);
		   // this.saveUserCaptureScreenImage(userId);
			 saveScreenCapturesLocal(image,userId);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			log.error("CaptureScreenshots -> saveUserCaptureScreenImage()-> Exception ->"+ex);
			
		}
		finally{
			ImageIO.setUseCache(false);
		}
	}
	/**
	 * Created By Bhagya on march 18th 2021
	 * @param image
	 * @param userId
	 * 
	 * Service for to save the screenshots locally
	 */
	
	public void saveScreenCapturesLocal(BufferedImage image,Integer userId) {
	log.info(" CaptureScreenshots -> saveScreenCapturesLocal() ");
	System.out.println("Saving screenshots locally");
		String fileName=userId+"_"+Timestamp.valueOf(LocalDateTime.now()).toString().replace(":","#") + ".png";
		String filepath="C:\\ProgramData\\idlemonitor\\images\\"+fileName;
		try {
			  File destinationFile=new File(filepath);
			  if(!destinationFile.exists()){
				  destinationFile.mkdirs();
			  }
		    ImageIO.write(image, "png", destinationFile);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
}