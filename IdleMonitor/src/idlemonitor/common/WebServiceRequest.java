package idlemonitor.common;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;

import org.apache.log4j.Logger;

import idlemonitor.controller.CaptureScreenshots;
import idlemonitor.controller.RecordTimer;
import idlemonitor.utility.scheduler.SaveWorkHoursScheduler;

import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;
/***
 * 
 * @author Bhagya
 * Service class for to connect/request the web services 
 */
public class WebServiceRequest{
	private static final Logger log= Logger.getLogger(WebServiceRequest.class);
	
	public Boolean isJoptionMessageExist=false;
	private static JDialog dlg=null;
	private static RecordTimer recordTimer=new RecordTimer(null, null, null);
	public static Boolean stopScheduler=false;
	Boolean networkHandle=false;
	
		
	/**
	 * Created By BHagya 
	 * @param requestUrl
	 * @param inputJsonString
	 * @return
	 * 
	 * This service handling the Get request to the web service
	 *  * Modified By bhagya on September 10th, 2020
	 * Added the network handling dialog and conditions
	 * If service Request was not successful, showing network warning message
	 * And verifying the network is available by every 30 seconds
	 * once it is available ,requesting same service from the catch blocks
	 */
	public String GetRequest(String requestUrl,String inputJsonString,Integer userId){
		log.info(" WebServiceRequest -> GetRequest()");
		String result=null;
		try{
			URL url = new URL(requestUrl);
		
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
  		conn.setDoInput(true);
  		conn.setDoOutput(true);
		conn.setRequestMethod("GET");
  		conn.setRequestProperty("Accept", "application/json");
  		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
  		conn.setRequestProperty("Accept-Charset", "UTF-8");
  		log.info("GET  connection Url "+conn.getURL());
  		OutputStream os = conn.getOutputStream();
        os.write(inputJsonString.getBytes("UTF-8"));
        os.close();
		conn.connect();
  		if (conn.getResponseCode() != 200) {
  			throw new RuntimeException("Failed : HTTP error code : "
  					+ conn.getResponseCode());
  		}

  		BufferedReader br = new BufferedReader(new InputStreamReader(
  			(conn.getInputStream())));

  		String output=null;
  		
  		while ((output = br.readLine()) != null) {
  			result=output;
  			log.info(" Output from Server .... \n "+output);
  			System.out.println(output);
  		}
  		if(isJoptionMessageExist==true) {
  			this.closeDialog();
  			stopScheduler=false;
  			isJoptionMessageExist=false;
  			recordTimer.startRunning(userId);
  			
  			
		}
  		br.close();
  		conn.disconnect();
		}
		catch(ConnectException e){
			this.closeDialog();
			stopScheduler=true;
			isJoptionMessageExist=true;
			log.info(" WebServiceRequest -> GETRequest -> ConnectException ->"+e.getMessage()+" stacktrace "+e.getStackTrace());
			String message = "Either Your Network Problem or Server Refused Your Request.";
		    this.dialoghandling(message);
		    recordTimer.stopRunning();
	  		this.GetRequest(requestUrl, inputJsonString,userId);
  			
  		}
		catch (MalformedURLException e) {
			log.info(" WebServiceRequest -> GetRequest -> MalformedURLException ->"+e.getMessage());
	  		e.printStackTrace();

	  	  } 
		catch (IOException e) {
			this.closeDialog();
			stopScheduler=true;
			isJoptionMessageExist=true;
			log.info(" WebServiceRequest -> GetRequest -> IOException ->"+e.getMessage());
	  		String message = "You are not connected to network.Pleace check your network connection.";
		    this.dialoghandling(message);
		    System.out.println("request Url "+requestUrl);
		    recordTimer.stopRunning();
	  		this.GetRequest(requestUrl, inputJsonString,userId);

	  	  }
		catch (Exception e) {
	  		log.info(" WebServiceRequest -> GetRequest -> Exception ->"+e.getMessage());
	  		e.printStackTrace();

	  	  }
  		return result;
	}
	/**
	 * Created By Bhagya
	 * @param requestUrl
	 * @param inputJsonString
	 * @return
	 * 
	 * This Service handling the post request to the web service
	 * 
	 * Modified By bhagya on September 10th, 2020
	 * Added the network handling dialog and conditions
	 * If service Request was not successful, showing network warning message
	 * And verifying the network is available by every 30 seconds
	 * once it is available ,requesting same service from the catch blocks
	 */
	public String POSTRequest(String requestUrl,String inputJsonString,Integer userId) {
		log.info(" WebServiceRequest -> POSTRequest() ");
		
		String result=null;
		HttpURLConnection conn=null;
		try{
			URL url = new URL(requestUrl);
		
		 conn = (HttpURLConnection) url.openConnection();
  		conn.setDoInput(true);
  		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
  		conn.setRequestProperty("Accept", "application/json");
  		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
  		conn.setRequestProperty("Accept-Charset", "UTF-8");
  		log.info(" POST connection Url "+conn.getURL());
  		OutputStream os = conn.getOutputStream();
        os.write(inputJsonString.getBytes("UTF-8"));
        os.close();
  		conn.connect();
  		if (conn.getResponseCode() != 200) {
  			throw new RuntimeException("Failed : HTTP error code : "
  					+ conn.getResponseCode());
  		}

  		BufferedReader  br = new BufferedReader(new InputStreamReader(
  			(conn.getInputStream())));

  		String output=null;
  		
  		while ((output = br.readLine()) != null) {
  			result=output;
  			log.info(" Output from Server .... \n "+output);
  			
  			System.out.println(" output post request "+output);
  		}
  		br.close();
  		if(isJoptionMessageExist==true) {
  			
  			this.closeDialog();
  			stopScheduler=false;
  			recordTimer.startRunning(userId);
  			isJoptionMessageExist=false;
  			
		}
  		
		}
		catch(ConnectException e){
			this.closeDialog();
			stopScheduler=true;
			isJoptionMessageExist=true;
			recordTimer.stopRunning();
			log.info(" WebServiceRequest -> POSTRequest -> ConnectException ->"+e.getMessage() +" stacktrace "+e.getStackTrace());
			String message = "Either Your Network Problem or Server Refused Your Request.";
		    this.dialoghandling(message);
		  
	  		this.POSTRequest(requestUrl, inputJsonString,userId);
  		
  		}
		catch (MalformedURLException e) {
			log.info(" WebServiceRequest -> POSTRequest -> MalformedURLException ->"+e.getMessage());
	  		e.printStackTrace();
	  		
	  	  }
		catch (IOException e) {
			this.closeDialog();
			stopScheduler=true;
			isJoptionMessageExist=true;
			 recordTimer.stopRunning();
	  		log.info(" WebServiceRequest -> POSTRequest -> IOException ->"+e.getMessage());
	  		String message = "You are not connected to network.Pleace check your network connection.";
		    this.dialoghandling(message);
	  		this.POSTRequest(requestUrl, inputJsonString,userId); // if Service not able to request because of network error, after 30 seconds or based on user action. Again we are requesting the same service and checking the internet connection
	  		
	  	  }
		catch (Exception e) {
	  		log.info(" WebServiceRequest -> POSTRequest -> Exception ->"+e.getMessage());
	  		e.printStackTrace();
	  		
	  	  }
		finally {
	  		conn.disconnect(); 
		}
		return result;
	}
	
	/**
	 * Created By Bhagya
	 * @param params
	 * @param title
	 * @param timeout_ms
	 * @return
	 * 
	 * This service will handle the popup alert to user about the network connection
	 */
	public final static boolean showConfirmDialogWithTimeout(Object params, String title, int timeout_ms) {
		log.info(" WebServiceRequest -> showConfirmDialogWithTimeout() ");
		String[] options = {"OK"};
	    final JOptionPane msg = new JOptionPane(params, JOptionPane.WARNING_MESSAGE, JOptionPane.OK_OPTION,null, options , options[0]);
	     dlg = msg.createDialog(title);

	    msg.setInitialSelectionValue(JOptionPane.OK_OPTION);
	    dlg.setAlwaysOnTop(true);
	    dlg.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	    dlg.addComponentListener(new ComponentAdapter() {
	        @Override
	        public void componentShown(ComponentEvent e) {
	            super.componentShown(e);
	            final Timer t = new Timer(timeout_ms, new ActionListener() {
	                @Override
	                public void actionPerformed(ActionEvent e) {
	                    dlg.setVisible(false);
	                }

	            });
	            t.start();
	        }
	    });
	    dlg.setVisible(true);

	    Object selectedvalue = msg.getValue();
	   
	    if (null!=selectedvalue ) {
	    	if(selectedvalue.equals("OK")) {
	        return false;
	    	}	
	    	else {
		        return true;
		    }
	    } 
	    else {
	        return true;
	    }
	}
	/**
	 * Craeted By Bhagya on 10th Sep 2020
	 * @param message
	 * 
	 * If user doesn't click on OK button on network warning message, this message will show my 30 seconds and closed automatically 
	 */
	
	public void dialoghandling(String message) {
		log.info(" WebServiceRequest -> dialoghandling() ");
		if(dlg!=null || networkHandle==true) {
			dlg.setVisible(false);
		}
		JLabel lbmsg = new JLabel(message);
	    boolean dialogResult = showConfirmDialogWithTimeout(lbmsg, "Network Warning", 30 * 1000);
	    networkHandle=dialogResult;
	    if (dialogResult == false) {
	    	 //okay is selected
	    	dlg.setVisible(false);
	    }
	    else 
	    {
	    	dlg.setVisible(false);
	    	 //No action performed
	    	
	    
	    }
	}
	/**
	 * Created By Bhagya on 10th Sep, 2020
	 * By using this method , we are closing the network warning joption pane message
	 */
	public void closeDialog() {
		log.info(" WebServiceRequest -> closeDialog() ");
		if(dlg!=null) {
			dlg.setVisible(false);
		}
	}
	/**
	 * 
	 * Created By BHagya 
	 * Service for to stop the process of record timer
	 */
	public Boolean stopProcess() {
		log.info(" WebServiceRequest -> stopProcess() ");
		return false;
	}
	/**
	 * Created By bhagya
	 * Service for to stop the scheduler process
	 */
	public Boolean stopSchedulerProcess() {
		log.info(" WebServiceRequest -> stopSchedulerProcess() ");
		return stopScheduler;
	}
	/**
	 * Created By Bhagya On 03rd nov 2020
	 * Service for to stop the timer/pause the timer
	 * 	 */
	public void stopTimer() {
		log.info(" WebServiceRequest -> stopTimer() ");
		stopScheduler=true;
		recordTimer.stopRunning();
	
	}
	/**
	 * Created By Bhagya On 03rd nov 2020
	 * Service for to start the timer
	 * 	 */
	public void startTimer(Integer userId) {
		log.info(" WebServiceRequest -> startTimer() ");
		stopScheduler=false;
		recordTimer.startRunning(userId);
	
	}
	/**
	 * Created By Bhagya On Jan 18th, 2021
	 * @param requestUrl
	 * @param inputJsonString
	 * @param userId
	 * @return
	 * 
	 * Service for to save the work hours based on both offline and online condition
	 */
	
	public String SaveWorkHoursPOSTRequestForBothOfflineAndOnline(String requestUrl,String inputJsonString,Integer userId) {
		log.info(" WebServiceRequest -> SaveWorkHoursPOSTRequestForBothOfflineAndOnline() ");
		
		String result=null;
		HttpURLConnection conn=null;
		try{
			URL url = new URL(requestUrl);
		
		 conn = (HttpURLConnection) url.openConnection();
  		conn.setDoInput(true);
  		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
  		conn.setRequestProperty("Accept", "application/json");
  		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
  		conn.setRequestProperty("Accept-Charset", "UTF-8");
  		log.info(" POST connection Url "+conn.getURL());
  		OutputStream os = conn.getOutputStream();
        os.write(inputJsonString.getBytes("UTF-8"));
        os.close();
  		conn.connect();
  		if (conn.getResponseCode() != 200) {
  			throw new RuntimeException("Failed : HTTP error code : "
  					+ conn.getResponseCode());
  		}

  		BufferedReader  br = new BufferedReader(new InputStreamReader(
  			(conn.getInputStream())));

  		String output=null;
  		
  		while ((output = br.readLine()) != null) {
  			result=output;
  			log.info(" Output from Server .... \n "+output);
  			System.out.println(" output post request from scheduler -suceess "+output);
  		}
  		br.close();
  		
		}
		catch(ConnectException e){
		
			log.info(" WebServiceRequest ->  SaveWorkHoursPOSTRequestForBothOfflineAndOnline -> ConnectException ->"+e.getMessage() +" stacktrace "+e.getStackTrace());
			
			saveWorkHoursLocal(requestUrl, inputJsonString);
  		}
		catch (MalformedURLException e) {
			log.info(" WebServiceRequest ->  SaveWorkHoursPOSTRequestForBothOfflineAndOnline -> MalformedURLException ->"+e.getMessage());
	  		e.printStackTrace();
	  		
	  	  }
		catch (IOException e) {
			
	  		log.info(" WebServiceRequest ->  SaveWorkHoursPOSTRequestForBothOfflineAndOnline -> IOException ->"+e.getMessage());
	  		saveWorkHoursLocal(requestUrl, inputJsonString);
	  	  }
		catch (Exception e) {
	  		log.info(" WebServiceRequest ->  SaveWorkHoursPOSTRequestForBothOfflineAndOnline -> Exception ->"+e.getMessage());
	  		e.printStackTrace();
	  		
	  	  }
		finally {
	  		conn.disconnect(); 
		}
		return result;
	}
	/**
	 * Created by bhagya on jan 18th, 2021
	 * @param requestUrl
	 * @param inputJsonString
	 * 
	 * Service for to save the work hours in local file
	 */
	public void saveWorkHoursLocal(String requestUrl,String inputJsonString) {
		log.info("WebServiceRequest -> saveWorkHoursLocal() ");
		 File file=new File("C:\\ProgramData\\idlemonitor\\idlemonitorWorkHours.txt");
		 System.out.println(" inside saving work hours local" );
		try {
			if(file.exists()) {
				System.out.println("file deletion  saving to local" );
		        	file.deleteOnExit();
		     }
			 file.createNewFile();  //if the file !exist create a new one
             BufferedWriter bw = new BufferedWriter(new FileWriter(file.getAbsolutePath()));
            // bw.write(requestUrl); //write the request url
            // bw.newLine(); //leave a new Line
             bw.write(inputJsonString); //write the input json string
             bw.newLine(); //leave a new Line
           
             bw.close(); //close the BufferdWriter
			
		}
		catch( Exception e) {
			e.printStackTrace();
		}
	}
}