package idlemonitor.common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * 
 * @author Bhagya
 * Class for to read the configuration properties
 */
public class ReadConfigureProperties{
	
	private static final Logger log= Logger.getLogger(ReadConfigureProperties.class);
	/**
	 * Created By Bhagya
	 * @param fileName
	 * @return
	 * @throws IOException
	 * 
	 * Service for to read the properties file
	 */
	public static Properties readPropertiesFile(String fileName) throws IOException {
		log.info(" ReadConfigureProperties -> readPropertiesFile ()");
	      FileInputStream fis = null;
	      Properties prop = null;
	      try {
	         fis = new FileInputStream(fileName);
	         prop = new Properties();
	         prop.load(fis);
	      } 
	      catch(FileNotFoundException fnfe) {
	    	  log.info(" ReadConfigureProperties -> readPropertiesFile () -> FileNotFoundException ->  "+fnfe.toString());
	         fnfe.printStackTrace();
	      } 
	      catch(IOException ioe) {
	    	  log.info(" ReadConfigureProperties -> readPropertiesFile () -> IOException ->  "+ioe.toString());
	         ioe.printStackTrace();
	      }
	      catch(Exception e) {
	    	  log.info(" ReadConfigureProperties -> readPropertiesFile () -> Exception ->  "+e.toString());
	         e.printStackTrace();
	      } 
	      finally {
	         fis.close();
	      }
	      return prop;
	   }
	/**
	 * Created  By Bhagya
	 * @return
	 * 
	 * Service for to get the Base Url of web services request
	 */
	public String getBaseUrl(){
		log.info(" ReadConfigureProperties -> getBaseUrl() ");
    	String baseUrl="";
    	try {
    		InputStream in = ClassLoader.getSystemResourceAsStream("configure.properties");
    		Properties props = new Properties();
    		props.load(in);
    		in.close();
			baseUrl=props.getProperty("baseurl");
			System.out.println(" base url + 1 "+baseUrl);
		} 
    	catch (IOException e1) {
			log.info(" ReadConfigureProperties -> getBaseUrl() () -> IOException ->  "+e1.toString());
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	catch (Exception e1) {
			log.info(" ReadConfigureProperties -> getBaseUrl() () -> Exception ->  "+e1.toString());
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	return baseUrl;
    }
}