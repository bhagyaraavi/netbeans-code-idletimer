package idlemonitor.utility.scheduler;

import idlemonitor.appUIScreens.WorkedHoursIM;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;


/**
 * 
 * @author Bhagya
 *  Scheduler job class for save work hours and screenshots which was captured offline
 */
public class SaveOfflineDataJob implements Job
{
	private static final Logger log= Logger.getLogger(SaveOfflineDataJob.class);
	
	/**
	 * Created the Job execution method by bhagya
	 */
	public void execute(JobExecutionContext context) throws JobExecutionException {
		log.info(" SaveOfflineDataJob -> execute() ");
		WorkedHoursIM.saveOfflineWorkHoursToServer();
		WorkedHoursIM.saveOfflineScreenshotsToServer();
		
	}

}