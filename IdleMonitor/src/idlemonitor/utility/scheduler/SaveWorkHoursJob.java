package idlemonitor.utility.scheduler;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import idlemonitor.appUIScreens.WorkedHoursIM;
/**
 * 
 * @author Bhagya
 *  Scheduler job class for save work hours
 */
public class SaveWorkHoursJob implements Job
{
	private static final Logger log= Logger.getLogger(SaveWorkHoursJob.class);
	
	/**
	 * Created the Job execution method by bhagya
	 */
	public void execute(JobExecutionContext context) throws JobExecutionException {
		log.info(" SaveWorkHoursJob -> execute() ");
		//IdleMonitor.saveWorkHours(false);
		WorkedHoursIM.saveWorkHoursFromScheduler(false);
	}

}